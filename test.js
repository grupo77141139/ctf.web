const fs = require('fs');

function checkTextInHtml(filePath, expectedText) {
    const content = fs.readFileSync(filePath, 'utf-8');
    if (content.includes(expectedText)) {
        console.log(`Test passed: "${expectedText}" found in ${filePath}`);
    } else {
        console.error(`Test failed: "${expectedText}" NOT found in ${filePath}`);
        process.exit(1);  // Esto hará que el proceso termine con un error, lo cual será capturado por Jenkins.
    }
}

checkTextInHtml('./index.html', 'FORTIS FORTUNA ADIUVAT');
checkTextInHtml('./index.html', 'MEMENTO MORI');
checkTextInHtml('./index.html', 'MEMENTO VIVIRE');
